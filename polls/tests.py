import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Question


class QuestionModelTest(TestCase):
    def test_was_published_recently_with_future_question(self):
        '''
        was_published_recently() returns false for questions
        whose pub_date is in the future
        '''
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        '''
        was_published_recently() returns false for questions
        whose pub_date is older than 1 day
        '''
        time = timezone.now() - datetime.timedelta(days=5)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        '''
        was_published_recently() returns True for questions
        whose pub_date is less than 1 day old
        '''
        recent_question = Question(pub_date=timezone.now())
        self.assertIs(recent_question.was_published_recently(), True)


def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text,
                                   pub_date=time)


class QuestionsIndexViewTest(TestCase):
    def test_no_questions(self):
        '''
        if no questions are present should show appropriate message
        '''
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls found")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        '''
        if 1 question from past is present, should show it appropriately
        '''
        create_question(question_text='Past Question', days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past Question>']
        )

    def test_future_question(self):
        '''
        if question exists in future, should not display it
        '''
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls found")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_and_past_questions(self):
        '''
        if both future and past questions exist, show only past question
        '''
        create_question(question_text='Future Question', days=5)
        create_question(question_text='Past Question', days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past Question>']
        )

    def test_two_past_questions(self):
        '''
        show multiple questions from the past
        '''
        create_question(question_text='Past Question 1', days=-5)
        create_question(question_text='Past Question 2', days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past Question 1>', '<Question: Past Question 2>']
        )


class QuestionsDetailViewTest(TestCase):
    def test_future_question_detail_view(self):
        '''
        should give a 404 error if question has pub_date in future
        '''
        future_question = create_question(question_text='Future Question',
                                          days=10)
        response = self.client.get(reverse('polls:detail',
                                           args=(future_question.id,)))
        self.assertEqual(response.status_code, 404)

    def test_past_question_detail_view(self):
        '''
        should display the question and its choices
        '''
        past_question = create_question(question_text='Past Question', days=-5)
        past_question.choice_set.create(choice_text='Choice 1', votes=0)
        past_question.choice_set.create(choice_text='Choice 2', votes=0)
        response = self.client.get(reverse('polls:detail',
                                           args=(past_question.id,)))
        self.assertContains(response, past_question.question_text)
        self.assertContains(response, 'Choice 1')
        self.assertContains(response, 'Choice 2')
